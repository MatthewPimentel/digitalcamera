/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
abstract class DigitalCamera {
    private String make;
    private String model;
    private double megaPixels;

    public DigitalCamera(String make, String model, double megaPixels) {
        this.make = make;
        this.model = model;
        this.megaPixels = megaPixels;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public double getMegaPixels() {
        return megaPixels;
    }
 
    abstract String describeCamera();
 
}
