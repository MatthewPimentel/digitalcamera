
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class DigitalCameraSimulation {
   public static void main (String [] args){
       PointAndShootCamera canon = new PointAndShootCamera("Canon", "Powershot A590", 8.0, "16GB");
       PhoneCamera apple = new PhoneCamera("Apple", "Iphone", 6.0, "64GB");
       
       List<DigitalCamera> list = new ArrayList<DigitalCamera>();   
       list.add(canon);
       list.add(apple);
       
       for (int i = 0; i < list.size(); i++){
           System.out.println(list.get(i).describeCamera());
       }
       
       
   }
}
