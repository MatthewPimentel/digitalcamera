/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class PhoneCamera extends DigitalCamera{
    private String internalMemorySize;
    public PhoneCamera (String make, String model, double megaPixels, String internalMemorySize) {
        super(make, model, megaPixels);
        this.internalMemorySize = internalMemorySize;
    }
    
    @Override
    public String describeCamera(){
        return "DigitalCamera{" + "make=" + super.getMake() + ", model=" + super.getModel() + ", megaPixels=" + super.getMegaPixels() + ", internalMemorySize=" + internalMemorySize;
    }
}
