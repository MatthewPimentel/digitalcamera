/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matth
 */
public class PointAndShootCamera extends DigitalCamera{
    private String externalMemorySize;

    public PointAndShootCamera(String make, String model, double megaPixels, String externalMemorySize) {
        super(make, model, megaPixels);
        this.externalMemorySize = externalMemorySize;
    }
    
    @Override
    public String describeCamera(){
        return "DigitalCamera{" + "make=" + super.getMake() + ", model=" + super.getModel() + ", megaPixels=" + super.getMegaPixels() + ", ExternalMemorySize=" + externalMemorySize;
    }
}
